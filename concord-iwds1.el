;;; concord-iwds1.el --- IWDS1 reader for CHISE-IPLD. -*- coding: utf-8-mcs-er -*-

;; Copyright (C) 2018,2019 MORIOKA Tomohiko

;; Author: MORIOKA Tomohiko <tomo@kanji.zinbun.kyoto-u.ac.jp>
;; Keywords: IWDS-1, 漢字 (CJKV Ideographs), UCS, character ontology, IPLD, IPFS

;; This file is a part of CHISE-IPLD

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

(require 'xml)
(require 'ids)
(require 'chise-ipld)

(concord-assign-genre 'code-point "/usr/local/var/chise-ipld/db")
;; (concord-assign-genre 'domain "/usr/local/var/chise-ipld/db")
(concord-assign-genre 'coded-character "/usr/local/var/chise-ipld/db")
(concord-assign-genre 'glyph "/usr/local/var/chise-ipld/db")

;(defun iwds-parse-ucv (parsed-list)
;  (let ((cell (car parsed-list))
;	rest dest ret)
;    (cond
;     ((and (consp cell)
;	   (eq (car cell) 'ucv))
;      (setq rest (nthcdr 2 cell))
;      (while rest
;	(if (setq ret (iwds-parse-group rest))
;	    (setq dest 

(defvar glyphwiki-name-ccs-code-alist
  '(("u2ff1-u5b80-u4e2f" =ucs-var-001 #x2bcb8)
    ("u2ff1-u5b80-u4e30" =ucs-var-002 #x2bcb8)
    ("u2ff1-u5b80-u4e30-ue0101" =ucs-var-003 #x2bcb8)
    ("u2ff1-u5b80-u9fb6" =ucs-var-004 #x2bcb8)
    ("u5bb3-03-itaiji-007" =big5-cdp #x8BF6)
    ("u4491-var001" =ucs-var-001 #x4491)
    ))

(defun glyphwiki-decode-string (string)
  (let (ret)
    (cond
     ((setq ret (assoc string glyphwiki-name-ccs-code-alist))
      (decode-char (nth 1 ret)(nth 2 ret)
		   (not (eq (nth 1 ret) '=ucs))
		   t)
      )
     ((string-match "^u\\([0-9a-f]+\\)$" string)
      (decode-char '=ucs@JP/hanazono (string-to-int (match-string 1 string) 16))
      )
     ((string-match "^u\\([0-9a-f]+\\)-g$" string)
      (decode-char '=ucs@gb (string-to-int (match-string 1 string) 16))
      )
     ((string-match "^u\\([0-9a-f]+\\)-t$" string)
      (decode-char '=ucs@cns (string-to-int (match-string 1 string) 16))
      )
     ((string-match "^u\\([0-9a-f]+\\)-k$" string)
      (decode-char '=ucs@ks (string-to-int (match-string 1 string) 16))
      )
     ((string-match "^u\\([0-9a-f]+\\)-\\(var\\|itaiji\\)-\\([0-9][0-9][0-9]\\)$" string)
      (decode-char (intern (format "=ucs-%s-%s"
				   (match-string 2 string)
				   (match-string 3 string)))
		   (string-to-int (match-string 1 string) 16)
		   t t)
      )
     ((string-match "^aj1-\\([0-9]+\\)$" string)
      (decode-char '=adobe-japan1 (string-to-int (match-string 1 string)))
      )
     ((string-match "^koseki-\\([0-9]+\\)$" string)
      (or (decode-char '=koseki (string-to-int (match-string 1 string)))
	  (decode-char '=hanyo-denshi/ks (string-to-int (match-string 1 string))))
      )
     ((string-match "^cdp-\\([0-9a-f]+\\)$" string)
      (decode-char '=big5-cdp (string-to-int (match-string 1 string) 16))
      )
     ((string-match "^cdp-\\([0-9a-f]+\\)-\\(var\\|itaiji\\)-\\([0-9][0-9][0-9]\\)$"
		    string)
      (decode-char (intern (format "=big5-cdp-%s-%s"
				   (match-string 2 string)
				   (match-string 3 string)))
		   (string-to-int (match-string 1 string) 16)
		   t t)
      )
     )))

(defun ids-parse-elements (string)
  (let (ret dest)
    (while (setq ret (ids-parse-element string nil))
      (setq dest (cons (car ret) dest)
	    string (cdr ret)))
    (nreverse dest)))

(defun concord-iwds1-store-entry (entry)
  (let ((file-name-coding-system 'utf-8-mcs-er)
	id iwds1-code entry-alist components glyphs
	iwds1-cobj parsed-components structure ret ucs dest)
    (when (and (consp entry)
	       (eq (car entry) 'entry)
	       (equal (cdr (assq 'kind (nth 1 entry)))
		      "unifiable"))
      (setq id (cdr (assq 'id (nth 1 entry))))
      (setq iwds1-code
	    (if (string-match "^[0-9]+$" id)
		(string-to-int id)
	      (intern id)))
      (setq entry-alist (nthcdr 2 entry))
      (when (and (setq components (assq 'components entry-alist))
		 (setq components (nth 2 components))
		 ;; (setq jis (assq 'jis entry-alist))
		 ;; (setq jis (nth 2 jis))
		 (setq glyphs (assq 'glyphs entry-alist))
		 (setq glyphs (nth 2 glyphs)))
	(unless (setq iwds1-cobj (concord-decode-object
				  '=iwds-1 iwds1-code 'glyph))
	  (setq iwds1-cobj (concord-make-object
			    'glyph (intern (format "iwds1-%s" id))))
	  (concord-object-put iwds1-cobj '=iwds-1 iwds1-code))
	(setq parsed-components
	      (cond
	       ((eq iwds1-code 220)
		(list ?\u6bb3 (decode-char '=big5-cdp #x8BA1))
		)
	       ((eq iwds1-code 134)
		(list (decode-char '=big5-cdp-itaiji-002 #x8BDC)
		      (decode-char '=big5-cdp-itaiji-001 #x8BDC)
		      (decode-char '=gt-k 02039))
		)
	       ((eq iwds1-code 146)
		'(?\u5165 ?\u201A2 ?\u4EBA)
		)
	       ((eq iwds1-code 293)
		(list (decode-char '=big5-cdp #x8845)
		      (decode-char '=gt-k 02910)
		      (decode-char '=big5-cdp-itaiji-001 #x8845))
		)
	       ((eq iwds1-code 398)
		'(?\u5405 ?\u2BA4F)
		)
	       ((eq iwds1-code 47)
		'(?\u4E8C ?\u51AB)
		)
	       ((eq iwds1-code '352a)
		'(?\u2E329)
		)
	       ((eq iwds1-code 5)
		'(?\u34DE)
		)
	       ((eq iwds1-code 60)
		'(?\u5C3F)
		)
	       (t
		(setq dest nil)
		(dolist (element
			 (ids-parse-elements
			  (mapconcat (lambda (char)
				       (char-to-string
					(if (setq ret (encode-char char '=big5-pua))
					    (decode-char '=big5-cdp ret)
					  char)))
				     components "")))
		  (when (and (consp element)
			     (setq structure (assq 'ideographic-structure element))
			     (setq structure (cdr structure))
			     (setq ret
				   (chise-ipld-ideographic-structure-find-char
				    structure))
			     (null (cdr ret)))
		    (setq element (car ret)))
		  (when (and (characterp element)
			     (setq ucs (char-ucs element)))
		    (setq element (decode-char '=ucs ucs))
		    (if (setq ret (get-char-attribute element '=>ucs*))
			(setq element (decode-char '=ucs ret))))
		  (unless (memq element dest)
		    (setq dest (cons element dest))))
		(nreverse dest)
		)))
	(concord-object-put
	 iwds1-cobj
	 'name
	 (concat "<"
		 (mapconcat
		  (lambda (element)
		    (if (and (consp element)
			     (setq ret
				   (assq 'ideographic-structure element)))
			(ideographic-structure-to-ids (cdr ret))
		      (char-to-string element)))
		  parsed-components "/")
		 ">"))
	(concord-object-put
	 iwds1-cobj
	 '->unifiable-component@iwds-1
	 (mapcar (lambda (element)
		   (if (and (consp element)
			    (setq ret
				  (assq 'ideographic-structure element)))
		       (concord-decode-object
			'=ipld
			(chise-ipld-put-character-ideographic-structure
			 nil (cdr ret))
			'glyph)
		     (concord-decode-object
		      '=ipld
		      (chise-ipld-put-coded-character element)
		      'coded-character)))
		 parsed-components))
	))))

(defun concord-iwds1-read-xml-file (filename)
  (interactive "fIWDS XML File name :")
  (let* ((coding-system-for-read 'utf-8-mcs-er)
	 (iwds-parsed (xml-parse-file filename)))
    (dolist (group (cdddar iwds-parsed))
      (if (and (consp group)
	       (eq (car group) 'group))
	  (dolist (subgroup (nthcdr 2 group))
	    (if (and (consp subgroup)
		     (eq (car subgroup) 'subgroup))
		(dolist (entry (nthcdr 2 subgroup))
		  (concord-iwds1-store-entry entry))
	      (concord-iwds1-store-entry subgroup)))))))

(defun concord-iwds1-batch-read-xml-file ()
  (let ((file-name (pop command-line-args-left)))
    (concord-iwds1-read-xml-file file-name)))

(defun concord-iwds1-confluent ()
  (let ((file-name-coding-system 'utf-8-mcs-er)
	(reduced t)
	dest-alist result-alist chars rest-alist ret
	confl-name confl-id confl-cobj)
    (concord-for-each-object-in-feature
     (lambda (obj val)
       (setq chars
	     (mapcan (lambda (char-obj)
		       (concord-object-get char-obj 'character))
		     val))
       (cond
	((setq ret (assoc* chars dest-alist :test #'intersection))
	 (setcar ret (union chars (car ret)))
	 (setcdr ret (adjoin obj (cdr ret)))
	 )
	(t
	 (setq dest-alist
	       (cons (cons chars (list obj))
		     dest-alist))
	 ))
       nil)
     '->unifiable-component@iwds-1
     'glyph)
    (while reduced
      (setq reduced nil
	    rest-alist dest-alist
	    dest-alist nil)
      (dolist (cell rest-alist)
	(setq chars (car cell))
	(cond
	 ((setq ret (assoc* chars dest-alist :test #'intersection))
	  (setcar ret (union chars (car ret)))
	  (setcdr ret (union (cdr cell) (cdr ret)))
	  (setq reduced t)
	  )
	 (t
	  (setq dest-alist
		(cons (cons chars (cdr cell))
		      dest-alist))
	  ))))
    (dolist (cell dest-alist)
      (if (cdr (cdr cell))
	  (setq result-alist
		(cons (cons (car cell)
			    (sort (cdr cell)
				  (lambda (a b)
				    (setq a (iwds1-parse-code
					     (concord-object-get a '=iwds-1))
					  b (iwds1-parse-code
					     (concord-object-get b '=iwds-1)))
				    (or (< (car a)(car b))
					(and (= (car a)(car b))
					     (string< (symbol-name (cdr a))
						      (symbol-name (cdr b))))))))
		      result-alist))
	(dolist (char (car cell))
	  (put-char-attribute char
			      'abstract-glyph@iwds-1
			      (cdr cell)))))
    (dolist (cell result-alist)
      (setq confl-id
	    (intern
	     (mapconcat (lambda (cell)
			  (format "%s" (concord-object-get cell '=iwds-1)))
			(cdr cell) ",")))
      (setq confl-name
	    (concat "<"
		    (mapconcat (lambda (cell)
				 (concord-object-get cell 'name))
			       (cdr cell) "/")
		    ">"))
      (unless (setq confl-cobj (concord-decode-object
				'=iwds-1@confluented confl-id 'glyph))
	(setq confl-cobj (concord-make-object
			  'glyph (intern (format "iwds1-%s" confl-id))))
	(concord-object-put confl-cobj '=iwds-1@confluented confl-id))
      (concord-object-put confl-cobj 'name confl-name)
      (concord-object-put confl-cobj '->confluented (cdr cell))
      (dolist (char (car cell))
	(concord-object-put
	 (concord-decode-object
	  '=ipld
	  (chise-ipld-put-coded-character char)
	  'coded-character)
	 '<-unifiable-component@iwds-1/confluented
	 (list confl-cobj))
	(put-char-attribute char
			    'abstract-glyph@iwds-1
			    (list confl-cobj)))
      ))
  (save-char-attribute-table 'abstract-glyph@iwds-1))

(defun concord-iwds1-bind-abstract-character-to-abstract-glyph ()
  (mount-char-attribute-table 'abstract-glyph@iwds-1)
  (let ((file-name-coding-system 'utf-8-mcs-er)
	ret char id)
    (concord-for-each-object-in-feature
     (lambda (obj val)
       (setq char nil)
       (unless (and (integerp val)
		    (setq char (decode-char '=>iwds-1 val)))
	 (setq ret (concord-object-get obj '->unifiable-component@iwds-1))
	 (when (null (cdr ret))
	   (when (and (setq char (concord-object-get (car ret) 'character))
		      (null (cdr char)))
	     (setq char (car char))
	     (when (integerp val)
	       (put-char-attribute char '=>iwds-1 val))
	     )))
       (when char
	 (put-char-attribute char 'abstract-glyph@iwds-1 (list obj))
	 (concord-object-put obj 'character (list char))
	 (when (setq ret (concord-object-get obj '<-confluented))
	   (put-char-attribute char 'abstract-glyph@iwds-1/confluented ret))
	 )
       nil)
     '=iwds-1
     'glyph)

    (map-char-attribute
     (lambda (c v)
       (when (setq ret (split-string (car (split-string (car v) ";")) "\\+"))
	 (setq id (intern
		   (mapconcat
		    (lambda (cell)
		      (format "%d%s"
			      (car cell)
			      (or (cdr cell)
				  "")))
		    (sort (mapcar #'iwds1-parse-code ret)
			  (lambda (a b)
			    (or (< (car a)(car b))
				(and (= (car a)(car b))
				     (string< (symbol-name (cdr a))
					      (symbol-name (cdr b)))))))
		    ",")))
	 (cond ((setq ret (concord-decode-object '=iwds-1@confluented id 'glyph))
		(put-char-attribute c 'abstract-glyph@iwds-1/confluented (list ret))
		(concord-object-put ret 'character (list c))
		)
	       ((setq ret (concord-decode-object '=iwds-1 id 'glyph))
		(put-char-attribute c 'abstract-glyph@iwds-1 (list ret))
		(concord-object-put ret 'character (list c))
		))
	 )
       nil)
     '=>iwds-1*note)
    )
  (save-char-attribute-table 'abstract-glyph@iwds-1)
  (save-char-attribute-table 'abstract-glyph@iwds-1/confluented))

(defun iwds1-parse-code (code)
  (cond ((numberp code)
	 (list code)
	 )
	((symbolp code)
	 (setq code (symbol-name code))
	 (if (string-match "^\\([0-9]+\\)" code)
	     (cons (string-to-int (match-string 1 code))
		   (let ((ret (substring code (match-end 1))))
		     (if (string= "" ret)
			 nil
		       (intern ret))))
	   ))
	((stringp code)
	 (if (string-match "^\\([0-9]+\\)" code)
	     (cons (string-to-int (match-string 1 code))
		   (let ((ret (substring code (match-end 1))))
		     (if (string= "" ret)
			 nil
		       (intern ret))))
	   ))
	))

(defun ideographic-structure-get-iwds1-abstract-form (structure)
  (let (ret ret2 char cobj)
    (mapcar
     (lambda (component)
       (when (and (consp component)
		  (setq ret (find-char component)))
	 (setq component ret))
       (cond ((characterp component)
	      (setq ret
		    (or (char-feature component 'abstract-glyph@iwds-1/confluented)
			(char-feature component 'abstract-glyph@iwds-1)))
	      (cond (ret
		     (setq char (concord-object-get (car ret) 'character))
		     (if char
			 (car char)
		       (car ret))
		     )
		    ((setq ret (char-feature component 'ideographic-structure))
		     (setq ret2 (ideographic-structure-get-iwds1-abstract-form ret))
		     (if (equal ret ret2)
			 component
		       (setq ret (chise-ipld-ideographic-structure-find-char ret2))
		       (if ret
			   (car ret)
			 (when (setq ret (char-ucs component))
			   (setq char (decode-char '=ucs ret))
			   (when (setq ret (get-char-attribute char '=>ucs*))
			     (setq char (decode-char '=ucs ret)))
			   (setq component char))
			 (cond
			  ((and (setq ret
				      (chise-ipld-put-character-ideographic-structure
				       component ret2
				       '->ideographic-structure@abstract/iwds-1))
				(setq cobj (concord-decode-object '=ipld ret 'glyph)))
			   (put-char-attribute component
					       'abstract-glyph@iwds-1
					       (list cobj))
			   cobj)
			  (t
			   (list (cons 'ideographic-structure ret2))
			   ))))
		     )
		    (t
		     component))
	      )
	     ((and (consp component)
		   (setq ret (assq 'ideographic-structure component)))
	      (setq ret2 (ideographic-structure-get-iwds1-abstract-form (cdr ret)))
	      (setq ret (chise-ipld-ideographic-structure-find-char ret2))
	      (if ret
		  (car ret)
		(cond
		 ((and (setq ret
			     (chise-ipld-put-character-ideographic-structure
			      nil ret2))
		       (setq cobj (concord-decode-object '=ipld ret 'glyph)))
		  cobj)
		 (t
		  (list (cons 'ideographic-structure ret2))
		  )))
	      )))
     (chise-ipld-compose-ideographic-structure structure))))

(defun concord-iwds1-normalize-ideographic-structures ()
  (mount-char-attribute-table 'abstract-glyph@iwds-1)
  (mount-char-attribute-table 'abstract-glyph@iwds-1/confluented)

  (let ((terminal-coding-system 'utf-8-mcs-er)
	(file-name-coding-system 'utf-8-mcs-er)
	ret2)
    (map-char-attribute
     (lambda (c v)
       (unless (get-char-attribute c 'abstract-glyph@iwds-1)
	 (princ (format "%c (%04X) : %S" c c v))
	 (setq ret2 (ideographic-structure-get-iwds1-abstract-form v))
	 (unless (equal v ret2)
	   (princ (format " -> %S" ret2))
	   (put-char-attribute c 'abstract-glyph@iwds-1
			       (list
				(concord-decode-object
				 '=ipld
				 (chise-ipld-put-character-ideographic-structure
				  c ret2
				  '->ideographic-structure@abstract/iwds-1)
				 'glyph))))
	 (princ "\n")
	 )
       nil)
     'ideographic-structure))

  (save-char-attribute-table 'abstract-glyph@iwds-1))


;;; @ End.
;;;

(provide 'concord-iwds1)

;;; concord-iwds1.el ends here
