;;; ipld.el --- IPLD wrapper for GNU Emacsen

;; Copyright (C) 2018,2019 MORIOKA Tomohiko

;; Author: MORIOKA Tomohiko <tomo@kanji.zinbun.kyoto-u.ac.jp>
;; Keywords: IPLD, IPFS, alist

;; This file is a part of Kanbun-IPLD.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;;; Code:

(require 'json)

(when (featurep 'chise)
(defun json-encode-char0 (char ccs)
  (or (encode-char char '=ucs@JP)
      (encode-char char '=>ucs@JP)))
  )

(defun ipld-put (object)
  (with-temp-buffer
    (insert (json-encode object))
    (call-process-region (point-min)(point-max) "ipfs" t t nil "dag" "put")
    (goto-char (point-min))
    (let ((case-fold-search nil))
      (if (search-forward "Error" nil t)
	  (error (buffer-substring (point-min)(point-at-eol)))
	(buffer-substring (point-min)(point-at-eol))))))

(defun ipld-get (cid)
  (json-read-from-string
   (with-temp-buffer
     (call-process "ipfs" nil t nil "dag" "get" cid)
     (buffer-substring (point-min)(point-max)))))

(defun ipld-pin-add (cid)
  (with-temp-buffer
    (call-process "ipfs" nil t nil "pin" "add" cid)
    (buffer-substring (point-min)(point-max))))

(defun ipld-pin-rm (cid)
  (with-temp-buffer
    (call-process "ipfs" nil t nil "pin" "rm" cid)
    (buffer-substring (point-min)(point-max))))


;;; @ End.
;;;

(provide 'ipld)

;;; ipld.el ends here
