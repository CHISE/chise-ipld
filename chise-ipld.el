;;; chise-ipld.el --- Ideographic Structure (IDS) database based on IPLD. -*- coding: utf-8-mcs-er -*-

;; Copyright (C) 2018,2019 MORIOKA Tomohiko

;; Author: MORIOKA Tomohiko <tomo@kanji.zinbun.kyoto-u.ac.jp>
;; Keywords: IDS, IDC, 漢字 (CJKV Ideographs), UCS, character ontology, IPLD, IPFS

;; This file is a part of CHISE-IPLD

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

(require 'ipld)
(require 'concord-turtle-dump)

(concord-assign-genre 'code-point "/usr/local/var/chise-ipld/db")
;; (concord-assign-genre 'domain "/usr/local/var/chise-ipld/db")
(concord-assign-genre 'coded-character "/usr/local/var/chise-ipld/db")
(concord-assign-genre 'glyph "/usr/local/var/chise-ipld/db")

(defun chise-ipld-put-ccs-code-point (ccs code-point
					  &optional ccs-base granularity domain)
  (let ((file-name-coding-system 'utf-8-mcs-er)
	ccs-base-id-f
	cpos-cid cpos-cobj domain-cid
	coded-char-cid coded-char-cobj)
    (if (setq coded-char-cobj (concord-decode-object
			       ccs code-point 'coded-character))
	(setq coded-char-cid (concord-object-get coded-char-cobj '=ipld))
      (unless ccs-base
	(let ((ret (chise-split-ccs-name ccs)))
	  (setq ccs-base (pop ret)
		granularity (pop ret)
		domain (car ret))))
      (setq ccs-base-id-f (intern (format "=%s" ccs-base)))
      (if (setq cpos-cobj (concord-decode-object
			   ccs-base-id-f code-point 'code-point))
	  (setq cpos-cid (concord-object-get cpos-cobj '=ipld))
	(setq cpos-cid (ipld-put (list (cons ccs-base code-point))))
	(setq cpos-cobj (concord-make-object 'code-point (intern cpos-cid)))
	(concord-object-put cpos-cobj '=ipld cpos-cid)
	(concord-object-put cpos-cobj ccs-base-id-f code-point))

      (setq coded-char-cid
	    (ipld-put
	     (list
	      (cons 'granularity granularity)
	      (cons (intern (format "%s-of" granularity))
		    (list*
		     (cons 'code-point (list (cons '/ cpos-cid)))
		     (when domain
		       (setq domain-cid
			     (ipld-put (list (cons 'CHISE-domain domain))))
		       (list (cons 'context (list (cons '/ domain-cid))))))))))
      ;; (ipld-pin-add coded-char-cid)
      (setq coded-char-cobj (concord-make-object
			     'coded-character (intern coded-char-cid)))
      (concord-object-put coded-char-cobj '=ipld coded-char-cid)
      (concord-object-put coded-char-cobj
			  (intern (format "<-%s" granularity))
			  (list cpos-cobj))
      (concord-object-put coded-char-cobj ccs code-point))
    (when code-point
      (let ((c (decode-char ccs code-point)))
	(concord-object-put coded-char-cobj 'character (list c))
        ;; (concord-object-put coded-char-cobj 'name (char-to-string c))
	(concord-object-put coded-char-cobj 'name
			    (cond ((eq ccs '=ucs)
				   (format "[U+%04X]" code-point)
				   )
				  (t
				   (format "[%s:%d(0x%X)]" ccs code-point code-point)
				   )))
	))
    coded-char-cid))

;;;###autoload
(defun chise-ipld-put-coded-character (character)
  (let ((file-name-coding-system 'utf-8-mcs-er)
	(spec (char-attribute-alist character))
	ccs-spec non-ccs-spec
	ccs-code-point-cobjs cid ccs
	ccs-base ccs-granularity ccs-domain
	ret
	coded-char-cid coded-char-cobj
	ucs-cid cdp-cid
	name)
    (dolist (cell spec)
      (if (and (find-charset (car cell))
	       (integerp (cdr cell)))
	  (push cell ccs-spec)
	(push cell non-ccs-spec)))
    (unless ccs-spec
      (setq ccs-spec
	    (cond ((setq ret (encode-char character '=ucs))
		   (list (cons '=ucs ret))
		   )
		  ((setq ccs (car (split-char character)))
		   (list (cons ccs (encode-char character ccs)))
		   ))))
    (if (and (setq ret (concord-decode-object
			(car (car ccs-spec))
			(cdr (car ccs-spec)) 'coded-character))
	     (setq coded-char-cobj
		   (car (last (concord-object-get ret '<-unified))))
    	     (null
	      (set-difference
	       (mapcar (lambda (obj)
			 (when (setq name (concord-object-get obj 'name))
			   (setq name
				 (cond ((string-match ":" name)
					(intern (substring name 1 (match-beginning 0)))
					)
				       ((string-match "^\\[U\\+" name)
					'=ucs)))
			   (cons name (concord-object-get obj name))))
		       (concord-object-get coded-char-cobj '->unified))
	       ;; (mapcar (lambda (cell)
	       ;;           (setq name (car cell)
	       ;;                 cid (cdr (nth 1 cell)))
	       ;;           (cons name
	       ;;                 (concord-object-get
	       ;;                  (concord-decode-object '=ipld cid 'coded-character)
	       ;;                  name)))
	       ;;         (cdr (assq 'unify (ipld-get coded-char-cid))))
	       ccs-spec :test #'equal)))
	(setq coded-char-cid (concord-object-get coded-char-cobj '=ipld))
      (setq coded-char-cid
	    (ipld-put
	     (list
	      (cons 'unify
		    (mapcar
		     (lambda (cell)
		       (setq ccs (car cell))
		       (setq ret (chise-split-ccs-name ccs))
		       (setq ccs-base (pop ret)
			     ccs-granularity (pop ret)
			     ccs-domain (car ret))
		       (setq cid (chise-ipld-put-ccs-code-point
				  ccs (cdr cell)
				  ccs-base ccs-granularity ccs-domain))
		       (cond ((eq ccs '=ucs)
			      (setq ucs-cid cid)
			      )
			     ((eq ccs '=big5-cdp)
			      (setq cdp-cid cid)
			      ))
		       (push (concord-decode-object '=ipld cid 'coded-character)
			     ccs-code-point-cobjs)
		       (cons ccs (list (cons '/ cid))))
		     ccs-spec)))))
      (unless (setq coded-char-cobj (concord-decode-object
				     '=ipld coded-char-cid 'coded-character))
	(setq coded-char-cobj (concord-make-object
			       'coded-character (intern coded-char-cid)))
	(concord-object-put coded-char-cobj
			    '->unified ccs-code-point-cobjs)
	(concord-object-put coded-char-cobj 'character (list character))
	(concord-object-put coded-char-cobj 'name (char-to-string character))
	(concord-object-put coded-char-cobj '=ipld coded-char-cid)
	)
      (when (or ucs-cid cdp-cid)
	(concord-object-put
	 coded-char-cobj
	 '->ucs+cdp
	 (list (or (concord-decode-object '=ipld ucs-cid 'coded-character)
		   (concord-decode-object '=ipld cdp-cid 'coded-character) ))))
      (ipld-pin-add coded-char-cid)
      )
    coded-char-cid))

(defun chise-ipld-put-abstract-glyph-component (glyph)
  (let ((ipld (concord-object-get glyph '=ipld))
	rules ret)
    (cond (ipld)
	  ((numberp (setq ret (concord-object-get glyph '=iwds-1)))
	   (chise-ipld-put-ccs-code-point '=>iwds-1 ret)
	   )
	  ((setq rules (concord-object-get glyph '->confluented))
	   (setq ipld
		 (ipld-put
		  (mapvector
		   (lambda (rule)
		     (unless (setq ret (concord-object-get rule '=ipld))
		       (setq ret
			     (ipld-put
			      (list
			       (cons 'iwds-1
				     (concord-object-get rule '=iwds-1)))))
		       (when ret
			 (concord-object-put rule '=ipld ret)))
		     (list (cons "/" ret)))
		   rules)))
	   (when ipld
	     (concord-object-put glyph '=ipld ipld))
	   ipld))))

(defun chise-ipld-put-component (component)
  (cond ((consp component)
	 (chise-ipld-put-character-ideographic-structure
	  nil (cdr (assq 'ideographic-structure component)))
	 )
	((characterp component)
	 (chise-ipld-put-coded-character component)
	 )
	(t
	 (chise-ipld-put-abstract-glyph-component component)
	 )))

(defun chise-ipld-put-character-ideographic-structure (character &optional ids-list
								 structure-feature)
  (unless ids-list
    (setq ids-list (get-char-attribute character 'ideographic-structure)))
  (unless structure-feature
    (setq structure-feature '->ideographic-structure))
  (let ((file-name-coding-system 'utf-8-mcs-er)
	char-cid char-cobj c-char-cobj
	idc idc-str
	p1 p2 p3
	pf1 pf2 pf3
	c1 c2 c3
	c1-cid c2-cid c3-cid
	c1-cobj c2-cobj c3-cobj
	cc1-cid cc2-cid cc3-cid
	cc1-cobj cc2-cobj cc3-cobj
	ret
	is-cid is-cobj
	c-is-cid c-is-cobj)
    (when ids-list
      (setq idc (car ids-list))
      (setq c1 (nth 1 ids-list)
	    c2 (nth 2 ids-list)
	    c3 (nth 3 ids-list))
      (if (char-ref-p idc)
	  (setq idc (plist-get idc :char)))
      (if (and (consp idc)
	       (setq ret (find-char idc)))
	  (setq idc ret))
      (if (and (consp c1)
	       (setq ret (find-char c1)))
	  (setq c1 ret))
      (if (and (consp c2)
	       (setq ret (find-char c2)))
	  (setq c2 ret))
      (if (and (consp c3)
	       (setq ret (find-char c3)))
	  (setq c3 ret))
      (cond
       ((eq idc ?\u2FF0) ; ⿰
	(setq p1 'left
	      p2 'right)
	)
       ((eq idc ?⿱)
	(setq p1 'above
	      p2 'below)
	)
       ((eq idc ?⿲)
	(setq p1 'left
	      p2 'middle
	      p3 'right)
	)
       ((eq idc ?⿳)
	(setq p1 'above
	      p2 'middle
	      p3 'below)
	)
       ((memq idc '(?⿴ ?⿵ ?⿶ ?⿷ ?⿸ ?⿹ ?⿺))
	(setq p1 'surround
	      p2 'filling)
	)
       ((eq idc ?⿻)
	(setq p1 'underlying
	      p2 'overlaying)
	)
       ((and idc (eq (encode-char idc '=ucs-itaiji-001) #x2FF6))
	(setq idc-str "SLR")
	(setq p1 'surround
	      p2 'filling)
	)
       ((and idc (eq (encode-char idc '=ucs-itaiji-001) #x2FF9))
	(setq idc-str "SRT")
	(setq p1 'surround
	      p2 'filling)
	)
       ((and idc (eq (encode-char idc '=ucs-var-001) #x2FF0))
	(setq idc-str "⿰・SLR")
	(setq p1 'left
	      p2 'right)
	)
       ((and idc (eq (encode-char idc '=>iwds-1) 307))
	(setq idc-str "⿰・⿺")
	(setq p1 'left
	      p2 'right)
	)
       ((and idc (eq (encode-char idc '=>iwds-1) 305))
	(setq idc-str "⿱・⿸")
	(setq p1 'above
	      p2 'below)
	)
       ((and idc (eq (encode-char idc '=>ucs@component) #x2FF5))
	(setq idc-str "⿱・⿵")
	(setq p1 'above
	      p2 'below)
	)
       )
      (cond
       (p3
	(unless idc-str
	  (setq idc-str (char-to-string idc)))
	(setq c1-cid (chise-ipld-put-component c1)
	      c2-cid (chise-ipld-put-component c2)
	      c3-cid (chise-ipld-put-component c3))
	(setq is-cid (ipld-put (list (cons 'operator idc-str)
				     (cons p1 (list (cons '/ c1-cid)))
				     (cons p2 (list (cons '/ c2-cid)))
				     (cons p3 (list (cons '/ c3-cid))))))
	(ipld-pin-add is-cid)
	(unless (setq is-cobj (concord-decode-object '=ipld is-cid 'glyph))
	  (setq is-cobj (concord-make-object 'glyph (intern is-cid)))
	  (concord-object-put is-cobj '=ipld is-cid)
	  (concord-object-put is-cobj 'operator idc-str)
	  (setq c1-cobj (or (concord-decode-object '=ipld c1-cid 'coded-character)
			    (concord-decode-object '=ipld c1-cid 'glyph)))
	  (setq c2-cobj (or (concord-decode-object '=ipld c2-cid 'coded-character)
			    (concord-decode-object '=ipld c2-cid 'glyph)))
	  (setq c3-cobj (or (concord-decode-object '=ipld c3-cid 'coded-character)
			    (concord-decode-object '=ipld c3-cid 'glyph)))
	  (setq pf1 (intern (format "->%s" p1)))
	  (setq pf2 (intern (format "->%s" p2)))
	  (setq pf3 (intern (format "->%s" p3)))
	  (concord-object-put is-cobj pf1 (list c1-cobj))
	  (concord-object-put is-cobj pf2 (list c2-cobj))
	  (concord-object-put is-cobj pf3 (list c3-cobj))
	  (concord-object-put is-cobj 'name
			      (format "%s%s%s%s"
				      (if (= (length idc-str) 1)
					  idc-str
					(concat "<" idc-str ">"))
				      (concord-object-get c1-cobj 'name)
				      (concord-object-get c2-cobj 'name)
				      (concord-object-get c3-cobj 'name)))
	  (when (and
		 (setq cc1-cid 
		       (when (setq cc1-cobj
				   (car (concord-object-get c1-cobj '->ucs+cdp)))
			 (concord-object-get cc1-cobj '=ipld)))
		 (setq cc2-cid
		       (when (setq cc2-cobj
				   (car (concord-object-get c2-cobj '->ucs+cdp)))
			 (concord-object-get cc2-cobj '=ipld)))
		 (setq cc3-cid
		       (when (setq cc3-cobj
				   (car (concord-object-get c3-cobj '->ucs+cdp)))
			 (concord-object-get cc3-cobj '=ipld))))
	    (setq c-is-cid (ipld-put (list (cons 'operator idc-str)
					   (cons p1 (list (cons '/ cc1-cid)))
					   (cons p2 (list (cons '/ cc2-cid)))
					   (cons p3 (list (cons '/ cc3-cid))))))
	    (ipld-pin-add c-is-cid)
	    (unless (setq c-is-cobj (concord-decode-object '=ipld c-is-cid 'glyph))
	      (setq c-is-cobj (concord-make-object 'glyph (intern c-is-cid)))
	      (concord-object-put c-is-cobj '=ipld c-is-cid)
	      (concord-object-put c-is-cobj 'operator idc-str)
	      (concord-object-put c-is-cobj pf1 (list cc1-cobj))
	      (concord-object-put c-is-cobj pf2 (list cc2-cobj))
	      (concord-object-put c-is-cobj pf3 (list cc3-cobj))
	      (concord-object-put c-is-cobj 'name
				  (format "%s%s%s%s"
					  (if (= (length idc-str) 1)
					      idc-str
					    (concat "<" idc-str ">"))
					  (concord-object-get cc1-cobj 'name)
					  (concord-object-get cc2-cobj 'name)
					  (concord-object-get cc3-cobj 'name)))
	      )
	    (concord-object-put is-cobj '->ucs+cdp (list c-is-cobj))
	    ))
	)
       (idc
	(unless idc-str
	  (setq idc-str (char-to-string idc)))
	(setq c1-cid (chise-ipld-put-component c1)
	      c2-cid (chise-ipld-put-component c2))
	(setq is-cid (ipld-put (list (cons 'operator idc-str)
				     (cons p1 (list (cons '/ c1-cid)))
				     (cons p2 (list (cons '/ c2-cid))))))
	(ipld-pin-add is-cid)
	(unless (setq is-cobj (concord-decode-object '=ipld is-cid 'glyph))
	  (setq is-cobj (concord-make-object 'glyph (intern is-cid)))
	  (concord-object-put is-cobj '=ipld is-cid)
	  (concord-object-put is-cobj 'operator idc-str)
	  (setq c1-cobj (or (concord-decode-object '=ipld c1-cid 'coded-character)
			    (concord-decode-object '=ipld c1-cid 'glyph)))
	  (setq c2-cobj (or (concord-decode-object '=ipld c2-cid 'coded-character)
			    (concord-decode-object '=ipld c2-cid 'glyph)))
	  (setq pf1 (intern (format "->%s" p1)))
	  (setq pf2 (intern (format "->%s" p2)))
	  (concord-object-put is-cobj pf1 (list c1-cobj))
	  (concord-object-put is-cobj pf2 (list c2-cobj))
	  (concord-object-put is-cobj 'name
			      (format "%s%s%s"
				      (if (= (length idc-str) 1)
					  idc-str
					(concat "<" idc-str ">"))
				      (concord-object-get c1-cobj 'name)
				      (concord-object-get c2-cobj 'name)))
	  (when (and
		 (setq cc1-cid 
		       (when (setq cc1-cobj
				   (car (concord-object-get c1-cobj '->ucs+cdp)))
			 (concord-object-get cc1-cobj '=ipld)))
		 (setq cc2-cid
		       (when (setq cc2-cobj
				   (car (concord-object-get c2-cobj '->ucs+cdp)))
			 (concord-object-get cc2-cobj '=ipld))))
	    (setq c-is-cid (ipld-put (list (cons 'operator idc-str)
					   (cons p1 (list (cons '/ cc1-cid)))
					   (cons p2 (list (cons '/ cc2-cid))))))
	    (ipld-pin-add c-is-cid)
	    (unless (setq c-is-cobj (concord-decode-object '=ipld c-is-cid 'glyph))
	      (setq c-is-cobj (concord-make-object 'glyph (intern c-is-cid)))
	      (concord-object-put c-is-cobj '=ipld c-is-cid)
	      (concord-object-put c-is-cobj 'operator idc-str)
	      (concord-object-put c-is-cobj pf1 (list cc1-cobj))
	      (concord-object-put c-is-cobj pf2 (list cc2-cobj))
	      (concord-object-put c-is-cobj 'name
				  (format "%s%s%s"
					  (if (= (length idc-str) 1)
					      idc-str
					    (concat "<" idc-str ">"))
					  (concord-object-get cc1-cobj 'name)
					  (concord-object-get cc2-cobj 'name)))
	      )
	    (concord-object-put is-cobj '->ucs+cdp (list c-is-cobj))
	    ))
	))
      (when is-cobj
	(when character
	  (setq char-cid (chise-ipld-put-coded-character character))
	  (setq char-cobj (concord-decode-object '=ipld char-cid 'coded-character))
	  (concord-object-put char-cobj structure-feature (list is-cobj))
	  (when c-is-cobj
	    (when (setq c-char-cobj (car (concord-object-get char-cobj '->ucs+cdp)))
	      (concord-object-put c-char-cobj
				  structure-feature (list c-is-cobj))))
	  ))
      is-cid)))

(defun chise-ipld-store-ideographic-structures ()
  (interactive)
  (let ((terminal-coding-system 'utf-8-mcs-er))
    (map-char-attribute
     (lambda (char val)
       (princ (format "Processing %c (%04X) ..." char char))
       (chise-ipld-put-character-ideographic-structure char val)
       (princ " done.\n")
       nil)
     'ideographic-structure)))


;;;###autoload
(defun chise-ipld-ideographic-structure-find-char (structure
						   &optional structure-rev-feature)
  (unless structure-rev-feature
    (setq structure-rev-feature '<-ideographic-structure))
  (let ((file-name-coding-system 'utf-8-mcs-er)
	dest)
    (dolist (coded-char (concord-object-get
			 (concord-decode-object
			  '=ipld (chise-ipld-put-character-ideographic-structure
				  nil structure)
			  'glyph)
			 structure-rev-feature))
      (setq dest (union dest (concord-object-get coded-char 'character)))
      )
    dest))

;;;###autoload
(defun chise-ipld-compose-ideographic-structure (structure)
  (let (ret)
    (mapcar (lambda (component)
	      (cond ((characterp component)
		     component)
		    ((and (consp component)
			  (setq ret (assq 'ideographic-structure component)))
		     (if (and (setq ret
				    (chise-ipld-ideographic-structure-find-char
				     (cdr ret)))
			      (null (cdr ret)))
			 (car ret)
		       component)
		     )
		    (t component)))
	    structure)))


;;; @ End.
;;;

(provide 'chise-ipld)

;;; chise-ipld.el ends here
